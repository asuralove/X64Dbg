#ifndef TRACEREGISTERS_H
#define TRACEREGISTERS_H

#include "RegistersView.h"
#include "RegistersViewA32.h"
#include "UVMGlobal.h"

class TraceRegistersA64 : public RegistersViewA64 {
  Q_OBJECT
 public:
  TraceRegistersA64(QWidget* parent = 0);

  void setRegisters(REGDUMP* registers);
  void setActive(bool isActive);
};

class TraceRegistersA32 : public RegistersViewA32 {
  Q_OBJECT
 public:
  TraceRegistersA32(QWidget* parent = 0);

  void setRegisters(REGDUMP* registers);
  void setActive(bool isActive);
};

#endif  // TRACEREGISTERS_H
