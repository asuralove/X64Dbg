#ifndef _VMPGLOBAL_H
#define _VMPGLOBAL_H

#define STUDIO_VERSION "1.4.2"
#define STUDIO_RELEASE 1

#define STUDIO_ENABLE_URANIUMVM 1
#define STUDIO_ENABLE_PHONEVMP 1
#define STUDIO_ENABLE_DECOMPILER 1

// release
#if STUDIO_RELEASE
#define STUDIO_BUILD_INFO ""
#else
// preview
#define STUDIO_BUILD_INFO " [Preview0 %3]"
#endif

#define UVMSVR_PORT 30334

#include <abstraction/DebugEngine.hpp>
using namespace dbgapi;

#include "../bridge/_global.h"

#define PlatformType DbgPlatformType

class VMPPluginManager {
 public:
  static VMPPluginManager *inst() {
    static VMPPluginManager mgr;
    return &mgr;
  }

  int loadADP();
  void unloadADP();

  // void sendEvent(adp_event_t event);
  // void sendEvent(adp_event_t event, void *ptr);

  // std::vector<ADPluginInstance> adps;

 protected:
  VMPPluginManager() {}
  ~VMPPluginManager() {}

  void loadADP(const QString &dir);

  // adp_api_t api;
  // adp_payload_t payload;
};

struct VMPManaFunc {
  mana::Function *func;
  umana32 rvastart;
  umana32 rvaend;

  VMPManaFunc() { reset(); }

  VMPManaFunc(const VMPManaFunc &f) {
    func = f.func;
    rvastart = f.rvastart;
    rvaend = f.rvaend;
  }

  VMPManaFunc &operator=(const VMPManaFunc &f) {
    func = f.func;
    rvastart = f.rvastart;
    rvaend = f.rvaend;
    return *this;
  }

  void reset() {
    func = nullptr;
    rvastart = 0;
    rvaend = 0;
  }

  bool thumb() const;
  bool operator==(const VMPManaFunc &f) const { return func == f.func; }
  operator bool() const { return func != nullptr; }
};

struct VMPManaDatabase {
  std::map<int, std::string> patches;  // patches for text opcodes
  std::map<uint64_t, std::string>
      dbgpatches;                 // patches for text opcodes when debugging
  QVariantMap usrdef;             // user define data like comment/bookmark
  QString path;                   // this module fullpath
  duint rtbase = 0;               // runtime base address
  mana::Binary *db = nullptr;     // analyzed database
  char *opcodes_cache = nullptr;  // text opcodes cached from file buffer
  int opcsize = 0;
  VMPManaFunc func_cache;              // the last operated function
  std::map<duint, const char *> strs;  // strings in file
  bool isvdb = false;

  void init(const QString &path, DbgModule *module);
  void close();
  ~VMPManaDatabase() { close(); }

  QString moduleName();
  QString moduleDir();
  QString udPath();

  const mana::Section *rvaToSection(duint rva) const;
  VMPManaFunc rvaToFunction(duint rva, bool eq);
  VMPManaFunc cvtFunction(mana::Function *mf);
  const mana::Insinfo *rvaToInstruction(const VMPManaFunc *func,
                                        duint rva) const;
};

class VMPGlobal {
 public:
  VMPGlobal();
  ~VMPGlobal();

  static VMPGlobal *inst() {
    static VMPGlobal global;
    return &global;
  }

  bool createDir(const QString &path);

  QString exePath();
  QString exeDir();
  QString tempDir();
  QString dataDir();
  QString adbDir();
  QString usrdataDir();
  QString decacheDir(const QString &triple);
  QString cfgPath();
  PlatformType currentPlatform();

  void updateUSBForward();

  unsigned pathCRC(const QString &path);
  QString pathModule(const QString &path);
  QString pathModuleMeta(const QString &path);
  bool hasModule(const QString &path);
  void addModule(const QString &path);
  VMPManaDatabase *getModule(const QString &path);
  QString *mapPath(const QString &path);

  void clearAll();
  Disassembler *diser(const ManaFunc &fn);
  Disassembler *diser(const mana::Function *fn);
  int addrSize();

  const char *addrName(addr_t addr);

 private:
  void analyzeStrings();

 public:
  std::map<unsigned, VMPManaDatabase *> modules;
  ParallelThread parallel;
  VMPManaDatabase *debugee = nullptr;
  std::map<uint64_t, std::string>
      dbgpatches;  // patches for memory when debugging
  std::map<uint64_t, DbgPageCache> dbgpages;  // pages for memory cache
  std::map<QString, QString> decaches;        // decache map from dyld cache
  QProcess usbmuxTBHttpSvr;
  QProcess usbmuxUVMSvr;

 private:
  Disassembler *diserA64 = nullptr;
  Disassembler *diserA32 = nullptr;
  Disassembler *diserT32 = nullptr;
  Disassembler *diserX64 = nullptr;
};

// run a shell command in the remote uvmserver side
QString svr_shell(const QString &cmds, bool result, bool timeout);

// get a remote file
QByteArray svr_getfile(const QByteArray &path, const QString &outpath = "");

// post a file to remote
void svr_postfile(const QString &path, const QByteArray &remotepath);

#endif  // _VMPGLOBAL_H
