#include "SelectSampleDialog.h"

#include "UVMTraceWidget.h"
#include "ui_SelectSampleDialog.h"

SelectSampleDialog::SelectSampleDialog(QWidget *parent)
    : QDialog(parent), ui(new Ui::SelectSampleDialog) {
  ui->setupUi(this);
  connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this,
          SLOT(onCurrentIndexChanged(int)));
  connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(onAddClicked()));

  for (size_t i = 0; i < uvmWin->mRecordInds.size(); i++) {
    ui->comboBox->addItem(QString("Index %1, %2 Chains")
                              .arg(i)
                              .arg(uvmWin->mRecordInds[i].size()));
  }
  ui->comboBox->setCurrentIndex(uvmWin->mRecordCur);
  ui->textEdit->append(QString("Current sample data index is %1.\n").arg(uvmWin->mRecordCur));
}

SelectSampleDialog::~SelectSampleDialog() { delete ui; }

void SelectSampleDialog::onCurrentIndexChanged(int index) {
  lastSelect = index;

  auto &inds = uvmWin->mRecordInds[index];
  std::set<addr_t> pcs;
  for (auto a : inds) {
    pcs.insert(a);
  }
  ui->textEdit->append(QString("Index : %1\nPC Chains : %2\nAddress Hit : %3\n")
                           .arg(index)
                           .arg(inds.size())
                           .arg(pcs.size()));
}

void SelectSampleDialog::onAddClicked() {
  allSelects.insert(lastSelect);
  ui->textEdit->append(QString("Added index %1 to combine.\n").arg(lastSelect));
}
