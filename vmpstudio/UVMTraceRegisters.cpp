#include "UVMTraceRegisters.h"

TraceRegistersA64::TraceRegistersA64(QWidget* parent)
    : RegistersViewA64(parent) {}

void TraceRegistersA64::setRegisters(REGDUMP* registers) {
  this->RegistersViewA64::setRegisters(registers);
}

void TraceRegistersA64::setActive(bool isActive) {
  this->isActive = isActive;
  this->RegistersViewA64::setRegisters(&this->wRegDumpStruct);
}

TraceRegistersA32::TraceRegistersA32(QWidget* parent)
    : RegistersViewA32(parent) {}

void TraceRegistersA32::setRegisters(REGDUMP* registers) {
  this->RegistersViewA32::setRegisters(registers);
}

void TraceRegistersA32::setActive(bool isActive) {
  this->isActive = isActive;
  this->RegistersViewA32::setRegisters(&this->wRegDumpStruct);
}
