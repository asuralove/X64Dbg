#ifndef SELECTSAMPLEDIALOG_H
#define SELECTSAMPLEDIALOG_H

#include <QDialog>
#include <set>

namespace Ui {
class SelectSampleDialog;
}

class SelectSampleDialog : public QDialog {
  Q_OBJECT

 public:
  explicit SelectSampleDialog(QWidget *parent = nullptr);
  ~SelectSampleDialog();

  int lastSelect = 0;
  std::set<int> allSelects;

 public slots:
  void onCurrentIndexChanged(int index);
  void onAddClicked();

 private:
  Ui::SelectSampleDialog *ui;
};

#endif  // SELECTSAMPLEDIALOG_H
