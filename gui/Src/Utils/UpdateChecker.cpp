#include "UpdateChecker.h"

#include <QDateTime>
#include <QDesktopServices>
#include <QIcon>
#include <QMessageBox>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>

#include "Bridge.h"
#include "MiscUtil.h"
#include "StringUtil.h"

UpdateChecker::UpdateChecker(QWidget* parent)
    : QObject(parent), mParent(parent) {
#if 0
  connect(this, SIGNAL(finished(QNetworkReply*)), this,
          SLOT(finishedSlot(QNetworkReply*)));
#endif
}

void UpdateChecker::checkForUpdates() {
  QDesktopServices::openUrl(QUrl("https://gitee.com/geekneo/A64Dbg"));
}

#if 0
void UpdateChecker::finishedSlot(QNetworkReply* reply) {
  if (reply->error() != QNetworkReply::NoError)  // error
  {
    SimpleErrorBox(mParent, tr("Network Error!"), reply->errorString());
    return;
  }
  QString json = QString(reply->readAll());
  reply->close();
  QRegExp regExp("\"published_at\": ?\"([^\"]+)\"");
  QDateTime serverTime;
  if (regExp.indexIn(json) >= 0)
    serverTime = QDateTime::fromString(regExp.cap(1), Qt::ISODate);
  if (!serverTime.isValid()) {
    SimpleErrorBox(mParent, tr("Error!"),
                   tr("File on server could not be parsed..."));
    return;
  }
  QRegExp regUrl("\"browser_download_url\": ?\"([^\"]+)\"");
  auto url =
      regUrl.indexIn(json) >= 0 ? regUrl.cap(1) : "http://releases.yunyoo.cn";
  auto server = serverTime.date();
  auto build = GetCompileDate();
  QString info;
  if (server > build)
    info = QString(tr("New build %1 available!<br>Download <a "
                      "href=.%2>here</a><br><br>You are now on build %3"))
               .arg(ToDateString(server))
               .arg(url)
               .arg(ToDateString(build));
  else if (server < build)
    info = QString(tr("You have a development build (%1) of x64dbg!"))
               .arg(ToDateString(build));
  else
    info = QString(tr("You have the latest build (%1) of x64dbg!"))
               .arg(ToDateString(build));
  SimpleInfoBox(mParent, tr("Information"), info);
}
#endif
