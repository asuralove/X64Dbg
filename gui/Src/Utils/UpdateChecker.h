#ifndef UPDATECHECKER_H
#define UPDATECHECKER_H

#include <QObject>

class UpdateChecker : public QObject {
  Q_OBJECT
 public:
  UpdateChecker(QWidget* parent);
  void checkForUpdates();

#if 0
 private slots:
  void finishedSlot(QNetworkReply* reply);
#endif

 private:
  QWidget* mParent;
};

#endif  // UPDATECHECKER_H
