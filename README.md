## X64Dbg

The GUI frontend project of A64Dbg and VMPStudio.

Support running on the following platforms:

 * Windows x86_64;
 * Intel macOS x86_64;
 * ARM macOS arm64;
 * Linux Ubuntu x86_64;
 * Linux Ubuntu arm64;

![ADARCH](https://gitee.com/geekneo/PantaDocumentRes/raw/master/a64dbg/adarch.png)

## Build

 * 1.open YunYooDbgCore.pro to build the abstract debug engine dynalic library;
 * 2.open gui/x64dbglib.pro to build the x64dbg common gui controls static library;
 * 3.open A64Dbg.pro to build the A64Dbg main program;
 * 4.open VMPStudio.pro to build the VMPStudio main program;
 * NOTE: on Windows, 2/3/4 need the extra qmake parameter, and must select the x64 build kit:
```
 QMAKE: -spec win32-clang-msvc
```


## DebugEngine

We make an abstraction layer for X64Dbg, you can implement the API in abstrasction/*.h to customize your debug core.


## License

See ./README.X64DBG.md and the modified GPL3 license at ./LICENSE.

